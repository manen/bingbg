# BingBG

BingBG is a package to get the current day's bing wallpaper.
That's basically it.

# Usage

```js
const getBackground = require("bingbg"); // It's a Promise, you can use async/await

getBackground().then(bg => {
    console.log("URL: " + bg.url);
    console.log("Time it took: " + bg.time);
});
```

##### (Note: Average time is about 500ms, so you will probably want to cache the result)
