const fetch = require("node-fetch");
const cheerio = require("cheerio");

async function get(debug) {
    const startTime = Date.now();

    var data = "";

    const res = await fetch("https://bing.com");

    const html = await res.text();
    const $ = cheerio.load(html);

    data = $(".img_cont")[0].attribs.style;

    data = data.substring(17);

    data = data.substring(5, data.length - 2);
    data = "https://bing.com" + data;

    const result = { url: data, time: Date.now() - startTime };

    if (debug) console.log(result);

    return result;
}

module.exports = get;
